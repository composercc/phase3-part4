package com.jin.service.impl;

import com.jin.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String sender;


    @Override
    public boolean sendCodeToMail(String mail, String code) {
        try {
            String content = "尊敬的用户您好，您的验证码为 " + code + " ,请妥善保管。";
            SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
            simpleMailMessage.setFrom(sender);
            simpleMailMessage.setTo(mail);
            simpleMailMessage.setText(content);
            simpleMailMessage.setSubject("Jin-验证码");
            javaMailSender.send(simpleMailMessage);
            return true;
        } catch (MailException e) {
            e.printStackTrace();
        }
        return false;
    }
}
