package com.jin.controller;

import com.jin.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("mail")
public class MailController {

    @Autowired
    MailService mailService;

    @RequestMapping("sendCodeToMail")
    @ResponseBody
    public boolean sendCodeToMail(String mail, String code) {
        return mailService.sendCodeToMail(mail, code);
    }

}
