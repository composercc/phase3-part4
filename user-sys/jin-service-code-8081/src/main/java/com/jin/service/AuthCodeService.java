package com.jin.service;

public interface AuthCodeService {

    boolean create(String email);

    int validate(String email, String code);

}
