package com.jin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jin.constant.CommonConstant;
import com.jin.entity.AuthCode;
import com.jin.mapper.AuthCodeMapper;
import com.jin.service.AuthCodeService;
import com.jin.service.MailServiceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class AuthCodeServiceImpl implements AuthCodeService {

    @Autowired
    AuthCodeMapper authCodeMapper;

    @Autowired
    MailServiceFeignClient mailServiceFeignClient;

    @Override
    public boolean create(String email) {
        AuthCode authCode = new AuthCode();
        String code = UUID.randomUUID().toString().substring(0, 6);
        authCode.setCode(code);
        authCode.setEmail(email);
        authCode.setCreateTime(System.currentTimeMillis());
        authCode.setExpireTime(System.currentTimeMillis() + CommonConstant.CODE_VALID_TIME);
        authCodeMapper.insert(authCode);
        try {
            // 发送邮件
            if (!mailServiceFeignClient.sendCodeToMail(email, code)) {
                throw new RuntimeException("发送邮件失败");
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("发送邮件失败");
        }
    }

    @Override
    public int validate(String email, String code) {
        QueryWrapper<AuthCode> qw = new QueryWrapper<>();
        qw.eq("email", email)
                .eq("code", code)
                .orderByDesc("expire_time");
        List<AuthCode> authCodes = authCodeMapper.selectList(qw);
        if (authCodes.size() > 0) {
            long expireTime = authCodes.get(0).getExpireTime();
            long validTime = System.currentTimeMillis();
            return expireTime < validTime ? 2 : 0;
        }
        return 1;
    }
}
