package com.jin.controller;

import com.jin.service.AuthCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("code")
public class CodeController {

    @Autowired
    AuthCodeService authCodeService;

    @RequestMapping("create")
    @ResponseBody
    public boolean create(String email) {
        return authCodeService.create(email);
    }

    @RequestMapping("validate")
    @ResponseBody
    public int validate(String email, String code) {
        return authCodeService.validate(email, code);
    }

}
