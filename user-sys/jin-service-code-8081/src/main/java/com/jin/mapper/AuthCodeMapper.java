package com.jin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jin.entity.AuthCode;

public interface AuthCodeMapper extends BaseMapper<AuthCode> {
}
