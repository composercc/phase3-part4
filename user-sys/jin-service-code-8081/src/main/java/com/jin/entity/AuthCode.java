package com.jin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class AuthCode {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String email;
    private String code;
    private Long createTime;
    private Long expireTime;

}
