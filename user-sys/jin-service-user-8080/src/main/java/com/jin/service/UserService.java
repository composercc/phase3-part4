package com.jin.service;

public interface UserService {

    boolean register(String email, String password, String code);

    boolean isRegister(String email);

    String login(String email, String password);

    String info(String token);

}
