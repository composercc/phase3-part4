package com.jin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jin.entity.Token;
import com.jin.entity.User;
import com.jin.mapper.TokenMapper;
import com.jin.mapper.UserMapper;
import com.jin.service.CodeServiceFeignClient;
import com.jin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    TokenMapper tokenMapper;

    @Autowired
    CodeServiceFeignClient codeServiceFeignClient;

    @Override
    public boolean register(String email, String password, String code) {
        int validate = codeServiceFeignClient.validate(email, code);
        if (validate == 0) {
            User user = new User();
            user.setEmail(email);
            user.setPassword(password);
            userMapper.insert(user);
            return true;
        }
        return false;
    }

    @Override
    public boolean isRegister(String email) {
        QueryWrapper<User> qw = new QueryWrapper<>();
        qw.eq("email", email);
        return userMapper.selectCount(qw) > 0;
    }

    @Override
    public String login(String email, String password) {
        QueryWrapper<User> qw = new QueryWrapper<>();
        qw.eq("email", email)
                .eq("password", password);
        if (userMapper.selectCount(qw) > 0) {
            Token token = new Token();
            token.setToken(UUID.randomUUID().toString());
            token.setEmail(email);
            tokenMapper.insert(token);
            return email;
        }
        return null;
    }

    @Override
    public String info(String token) {
        User info = tokenMapper.getInfoByToken(token);
        return info.getEmail();
    }
}
