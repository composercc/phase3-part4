package com.jin.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "jin-service-code")
@RequestMapping("code")
public interface CodeServiceFeignClient {

    @RequestMapping("create")
    boolean create(@RequestParam String email);

    @RequestMapping("validate")
    int validate(@RequestParam String email, @RequestParam String code);
}
