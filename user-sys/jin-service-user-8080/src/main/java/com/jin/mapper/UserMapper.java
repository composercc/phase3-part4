package com.jin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jin.entity.User;

public interface UserMapper extends BaseMapper<User> {
}
