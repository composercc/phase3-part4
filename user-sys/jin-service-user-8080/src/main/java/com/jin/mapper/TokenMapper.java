package com.jin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jin.entity.Token;
import com.jin.entity.User;
import org.apache.ibatis.annotations.Select;

public interface TokenMapper extends BaseMapper<Token> {

    @Select("SELECT u.* FROM `user` u \n" +
            "LEFT JOIN `token` t ON u.email = t.email \n" +
            "WHERE t.token = #{token}")
    User getInfoByToken(String token);

}
