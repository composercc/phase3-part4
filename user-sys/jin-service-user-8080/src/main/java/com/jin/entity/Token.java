package com.jin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class Token {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String email;
    private String token;

}
