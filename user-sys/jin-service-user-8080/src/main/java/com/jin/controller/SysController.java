package com.jin.controller;

import com.jin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SysController {

    @Value("${abc}")
    private String abc;

    @Autowired
    UserService userService;

    @ResponseBody
    @RequestMapping("abc")
    public String getAbc() {
        System.out.println("111111");
        return abc;
    }

    @RequestMapping("register")
    public String register() {
        return "register";
    }

    @RequestMapping("login")
    public String login() {
        return "login";
    }

    @RequestMapping("welcome")
    public ModelAndView welcome(String email, ModelAndView modelAndView) {
        modelAndView.addObject("email",email);
        modelAndView.setViewName("welcome");
        return modelAndView;
    }

}
