package com.jin.controller;

import com.jin.service.CodeServiceFeignClient;
import com.jin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    CodeServiceFeignClient codeServiceFeignClient;

    @ResponseBody
    @RequestMapping("qwe")
    public String qwe() {
        return "qwe";
    }

    @ResponseBody
    @RequestMapping("register")
    public boolean register(String email, String password, String code) {
        return userService.register(email, password, code);
    }

    @RequestMapping("isRegister")
    public boolean isRegister(String email) {
        return userService.isRegister(email);
    }

    @PostMapping("login")
    @ResponseBody
    public String login(String email, String password, HttpServletRequest request, HttpServletResponse response) {
        return userService.login(email, password);
    }

    @PostMapping("getAuthCode")
    @ResponseBody
    public boolean getAuthCode(String email) {
        return codeServiceFeignClient.create(email);
    }

    @PostMapping("codeValidate")
    @ResponseBody
    public int codeValidate(String email, String code) {
        return codeServiceFeignClient.validate(email,code);
    }

    @RequestMapping("info")
    public String info(String token) {
        return userService.info(token);
    }

}
